package br.ifsc.edu.aula_15_08;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

public class Main2Activity extends AppCompatActivity {
    private EditText edtPeso, edtAltura;
    private Button btnCalculo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        btnCalculo = (Button) findViewById(R.id.btnCalculo);
        edtPeso = (EditText) findViewById(R.id.edtPeso);
        edtAltura = (EditText) findViewById(R.id.edtAltura);
        ImageView  imagem = findViewById(R.id.imageView);
        imagem.setImageResource(R.drawable.perfil);

        btnCalculo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Integer peso = Integer.parseInt(edtPeso.getText().toString());
                Integer altura = Integer.parseInt(edtAltura.getText().toString());
                Integer imc = peso / (altura*2);

           Toast.makeText(getApplicationContext(), "IMC: "+ imc.toString(), Toast.LENGTH_LONG).show();

                if ( imc <= 18.5  ) {
                    Toast.makeText(getApplicationContext(), "Você está abaixo do peso " + imc, Toast.LENGTH_LONG).show();
                    ImageView abaixopeso = findViewById(R.id.imageView);
                    abaixopeso.setImageResource(R.drawable.abaixopeso);
                }


                else if ( imc >= 18.5 & imc <= 24.9 ){

                    Toast.makeText(getApplicationContext(), "Você está no peso ideal " + imc, Toast.LENGTH_LONG).show();
                    ImageView  normal = findViewById(R.id.imageView);
                    normal.setImageResource(R.drawable.normal);
                }

                else if ( imc >= 25 & imc <= 29.9 ){

                    Toast.makeText(getApplicationContext(), "Você está levemente acima do peso " + imc, Toast.LENGTH_LONG).show();
                    ImageView  sobrepeso = findViewById(R.id.imageView);
                    sobrepeso.setImageResource(R.drawable.sobrepeso);
                }

                else if ( imc >= 30 & imc <= 34.9 ){

                    Toast.makeText(getApplicationContext(), "Você está com obesidade grau I "+ imc, Toast.LENGTH_LONG).show();
                    ImageView  obesidade1 = findViewById(R.id.imageView);
                    obesidade1.setImageResource(R.drawable.obesidade1);
                }

                else if ( imc >= 35 & imc <= 39.9 ){

                    Toast.makeText(getApplicationContext(), "Você está com obesidade grau II (severa) "+ imc, Toast.LENGTH_LONG).show();
                    ImageView  obesidade2 = findViewById(R.id.imageView);
                    obesidade2.setImageResource(R.drawable.obesidade2);
                }

                else if ( imc >= 40 ){

                    Toast.makeText(getApplicationContext(), "Você está com obesidade grau III (mórbida) " + imc, Toast.LENGTH_LONG).show();
                    ImageView  obesidade3 = findViewById(R.id.imageView);
                    obesidade3.setImageResource(R.drawable.obesidade3);
                }
        }

        });

    }

}
