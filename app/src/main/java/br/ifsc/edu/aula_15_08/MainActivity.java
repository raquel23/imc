package br.ifsc.edu.aula_15_08;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void abreActivity(View view) {
        Intent i = new Intent(this, Main2Activity.class); //revela a intenção desta classe(this) para com a outra classe (main2act)
        startActivity(i); //dá start na intenção que declaramos acima

    }
}
